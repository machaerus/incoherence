[![forthebadge](https://forthebadge.com/images/badges/made-with-python.svg)](https://forthebadge.com) [![forthebadge](https://forthebadge.com/images/badges/60-percent-of-the-time-works-every-time.svg)](https://forthebadge.com)

# Incoherence Elimination

***

Code that was used to run experiments and generate graphs for my MSc thesis @ the University of Copenhagen.

**Title:** _Towards a Coherent Theory of Incoherent Beliefs_ 

**Topic:** Measuring and eliminating incoherence in Bayesian belief systems.