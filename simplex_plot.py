#!/usr/bin/env python

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import gridspec
import ternary
import imaging as img

from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)

plt.rc('text', usetex=True)
plt.rc('font', family='serif')

def scale(c) :
	"""Return perspective projection of belief function c
	onto the simplex in the direction of 0.
	Equivalent to scaling and to minimizing KLD/GKLD.
	"""
	return list(map(lambda x: x / np.sum(c), c))

def bayes(c) :
    return scale([c[0],0,c[2]])


tax = [None] * 3
fontsize = 15

fig = plt.figure(figsize=(10,5))
gs = gridspec.GridSpec(1,3)

ax1 = plt.subplot(gs[0,0])
figure1, tax[0] = ternary.figure(ax=ax1)
ax1.axis('off')
ax2 = plt.subplot(gs[0,1])
figure2, tax[1] = ternary.figure(ax=ax2)
ax2.axis('off')
ax3 = plt.subplot(gs[0,2])
figure3, tax[2] = ternary.figure(ax=ax3)
ax3.axis('off')

w0 = [1,0,0]
w1 = [0,1,0]
w2 = [0,0,1]

points = [
    [0.05, 0.5, 0.45],
    [0.15, 0.5, 0.35],
    [0.25, 0.5, 0.25],
    [0.35, 0.5, 0.15],
    [0.45, 0.5, 0.05],
]

########################################
# updating
# 1: brier
########################################

tax[0].clear_matplotlib_ticks()
tax[0].boundary()
tax[0].gridlines(multiple=0.2, color="black")
tax[0].set_title(r"a) IM, $\lambda = (.5, .5)$")

tax[0].plot( [tuple(w0),tuple(w1),tuple(w2)], marker='o', color='black', markeredgecolor='w', markeredgewidth=3, markersize=12 )
tax[0].annotate(r"$w_0$", (0.08,1,-0.1))
tax[0].annotate(r"$w_2$", (1.1,-0.1,-0.1))
tax[0].annotate(r"$w_1$", (-0.1,-0.1,1.2))
# for p in points :
#     tax[0].plot([tuple(p)], marker='o', color='black')
pointsx = list(map(lambda x: img.imaging(x,lmbd=[[1,0,0],[0.5,0,0.5],[0,0,1]]), points))
for i in range(len(points)) :
    tax[0].annotate("",
        (pointsx[i][0], pointsx[i][1], pointsx[i][2]),
        xytext=ternary.project_point((points[i][0], points[i][1], points[i][2])),
        xycoords='data',
        textcoords='data',
        arrowprops=dict(arrowstyle="->",
                        connectionstyle="arc3"))

ax1.set_ylim([-0.1, 1.1])
ax1.set_xlim([-0.2, 1.2])
ax1.set(adjustable='box-forced', aspect='equal')

########################################
# updating
# 2: lewis
########################################

tax[1].clear_matplotlib_ticks()
tax[1].boundary()
tax[1].gridlines(multiple=0.2, color="black")
tax[1].set_title(r"b) IM, $\lambda = (1, 0)$")

tax[1].plot( [tuple(w0),tuple(w1),tuple(w2)], marker='o', color='black', markeredgecolor='w', markeredgewidth=3, markersize=12 )
tax[1].annotate(r"$w_0$", (0.08,1,-0.1))
tax[1].annotate(r"$w_2$", (1.1,-0.1,-0.1))
tax[1].annotate(r"$w_1$", (-0.1,-0.1,1.2))
# for p in points :
#     tax[0].plot([tuple(p)], marker='o', color='black')
pointsx = list(map(lambda x: img.imaging(x,lmbd=[[1,0,0],[0,0,1],[0,0,1]]), points))
for i in range(len(points)) :
    tax[1].annotate("",
        (pointsx[i][0], pointsx[i][1],pointsx[i][2]),
        xytext=ternary.project_point((points[i][0], points[i][1], points[i][2])),
        xycoords='data',
        textcoords='data',
        arrowprops=dict(arrowstyle="->",
                        connectionstyle="arc3"))

ax2.set_ylim([-0.1, 1.1])
ax2.set_xlim([-0.2, 1.2])
ax2.set(adjustable='box-forced', aspect='equal')

########################################
# updating
# 3: bayes
########################################

tax[2].clear_matplotlib_ticks()
tax[2].boundary()
tax[2].gridlines(multiple=0.2, color="black")
tax[2].set_title(r"c) BC")

tax[2].plot( [tuple(w0),tuple(w1),tuple(w2)], marker='o', color='black', markeredgecolor='w', markeredgewidth=3, markersize=12 )
tax[2].annotate(r"$w_0$", (0.08,1,-0.1))
tax[2].annotate(r"$w_2$", (1.1,-0.1,-0.1))
tax[2].annotate(r"$w_1$", (-0.1,-0.1,1.2))
# for p in points :
#     tax[0].plot([tuple(p)], marker='o', color='black')
pointsx = list(map(bayes, points))
for i in range(len(points)) :
    tax[2].annotate("",
        (pointsx[i][0], pointsx[i][1],pointsx[i][2]),
        xytext=ternary.project_point((points[i][0], points[i][1], points[i][2])),
        xycoords='data',
        textcoords='data',
        arrowprops=dict(arrowstyle="->",
                        connectionstyle="arc3"))

ax3.set_ylim([-0.1, 1.1])
ax3.set_xlim([-0.2, 1.2])
ax3.set(adjustable='box-forced', aspect='equal')

gs.tight_layout(fig)

plt.savefig("/home/johann/Dropbox/praca licencjacka/szkic/images/sympleks.pdf", format="pdf", bbox_inches='tight')
plt.show()
