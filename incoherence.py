#!/usr/bin/env python

import numpy as np
import copy
from scipy.optimize import minimize

def norm(x, p) :
    """Return the p-norm of the vector x."""
    try :
        xp = map(lambda a: a**p, x)
        return sum(map(abs, xp)) ** (1./p)
    except TypeError :
        print("--- Provided arguments are invalid!")
        return None

def print_c(array):
    res = ""
    for e in array: res += "{:.4f} ".format(e)
    return res

def isCoherent(c, error=0.00001) :
    return not (any (t < -error for t in c) or
                any (t-1 > error for t in c) or
                abs(sum(c) - 1) > error)

def UIM(c, _E, verbose=True):
    '''_E is the list of indices of the possible worlds
    that we want to remove
    '''
    def log(s=None): 
        if verbose: 
            if s is not None: print(s)
            else: print()

    log("\n--- UIM --------------")
    x = copy.deepcopy(c)
    N = len(x)
    Delta = np.sum([x[i] for i in _E])
    delta = Delta / (N - len(_E))
    log("delta = {:.4f}".format(delta))
    for i in _E:
        x[i] = 0
    for i in (set(range(N)) - set(_E)):
        x[i] = x[i] + delta

    log("c = {}".format(print_c(x)))
    log("Phi \\ E: {}".format(str(_E)))
    log("----------------------\n")
    return x

def XUIM(c, Delta=None, _E=None, verbose=True) :
    '''One of the following arguments must be specified:
    - _E is the list of indices of the possible worlds
    that we want to remove
    - Delta is the surplus credence we want to redistribute
    '''
    def log(s=None): 
        if verbose: 
            if s is not None: print(s)
            else: print()

    if Delta is None and _E is None:
        raise ValueError("You need to specify either Delta or _E")

    x = copy.deepcopy(c)
    N = len(x)

    log("====================== XUIM =========================")
    log("Processing c = {}".format(print_c(x)))
    log("Worlds outside of E: {}".format(str(_E)))
    log()

    log(">> Step I - Redistribute")
    if _E is not None:
        x = UIM(x, _E, verbose=False)
    else:
        _E = []
        delta = Delta / N
        log("delta = {:.4f}".format(delta))
        for i in range(N):
            x[i] = x[i] + delta

    log("After Step I:")
    log("c = {}".format(print_c(x)))
    log("Sum of c = {:.4f}".format(np.sum(x)))
    log("Phi \\ E: {}".format(str(_E)))
    if isCoherent(x):
        log("Finished after Step I: credences coherent!\n")
        return x

    log("\n>> Step II - Normalize\n")
    # if credences are higher than 1, Delta is negative
    Delta = 1 - np.sum(x)
    delta = Delta / (N - len(_E))
    log("delta = {:.4f}".format(delta))
    for i in (set(range(N)) - set(_E)):
        x[i] = x[i] + delta

    log("After Step II:")
    log("c = {}".format(print_c(x)))
    log("Sum of c = {:.4f}".format(np.sum(x)))
    log("Phi \\ E: {}".format(str(_E)))
    if isCoherent(x):
        log("Finished after Step II: credences coherent!\n")
        return x

    log("\n>> Step III - Get rid of negatives\n")
    iteration = 0
    while True:
        log("Iteration: {}".format(iteration+1))
        for i in (set(range(N)) - set(_E)):
            if x[i] < 0:
                _E.append(i)
                x = UIM(x, _E, verbose=False)

                log("c = {}".format(print_c(x)))
                log("Sum of c = {:.4f}".format(np.sum(x)))
                log("Phi \\ E: {}".format(str(_E)))
                log()
                if isCoherent(x):
                    log("Finished after Step III: credences coherent!\n")
                    return x
                else:
                    iteration += 1
                    break

    log("We should never end up here!")


def kld(p, q, log='e') :
    if log == 'e' :
        return np.sum([ p[i] * np.log(p[i]/q[i]) for i in range(len(p))])
    elif log =='2' :
        return np.sum([ p[i] * np.log2(p[i]/q[i]) for i in range(len(p))])

def ikld(p, q, log='e'):
    return kld(q, p, log)

def gkld(p, q, log='e') :
    return kld(p, q, log) - np.sum(p) + np.sum(q)

def igkld(p, q, log='e') :
    return kld(q, p, log) - np.sum(q) + np.sum(p)

def pnorm(p, q, P):
    return norm(p - np.array(q), P)

def incoh(c, method='pnorm', p=2, log='e') :
    # dimension
    n = len(c)
    # convert to NumPy array
    c = np.array(c)
    # objective distance function to be minimized
    if method == 'pnorm' :
        D = lambda x: norm(c - np.array(x), p)
    elif method == 'gkld' :
        D = lambda x: gkld(x, c, log)
    elif method == 'igkld' :
        D = lambda x: igkld(x, c, log)
    elif method == 'kld' :
        D = lambda x: kld(x, c, log)
    elif method == 'ikld' :
        D = lambda x: ikld(x, c, log)
    # constraint function (only consider points from the simplex)
    constr = lambda x: np.sum(x) - 1
    # variables bounds - only allow points from [0;1]
    bnds = tuple((0,1) for i in range(n))
    # starting point for the algorithm - middle of the simplex
    x0 = [1./n for i in range(n)]
    # minimize objective function using
    # Sequential Least Squares Programming algorithm
    res = minimize( D, x0,
        method = 'SLSQP',
        bounds = bnds,
        constraints = { 'type': 'eq', 'fun': constr } )
    return res

def scale(c):
    '''Scaling is equivalent to applying XBC'''
    return map(lambda x: x / np.sum(c), c)

def incohImaging(c, lmbd, verbose=True) :
    x = copy.deepcopy(c)
    dim = len(x)

    allowed = [True] * dim # True means that that axis is still manipulable
    if verbose : print("Processing: c =", x, "lambda =", lmbd)
    if not isCoherent(x) :
        if verbose : print("Not coherent")
    else : return x

    if sum(x) != 1 :
        if sum(x) < 1:
            difference = 1 - sum(x)
            if verbose : print("Sum below 1")
            for i in range(dim) :
                x[i] = x[i] + difference * lmbd[i]
                if verbose : print(["{0:0.6f}".format(i) for i in x])
        elif sum(x) > 1:
            if verbose : print("Sum above 1")
            difference = sum(x) - 1
            for i in range(dim) :
                x[i] = x[i] - difference * ((1-lmbd[i])/(dim-sum(lmbd)))
                if verbose : print(["{0:0.6f}".format(i) for i in x])
        if verbose : print("Vector sums to 1")
        if not (any (t < 0 for t in x) or any (t > 1 for t in x)) :
            if verbose : print("Vector is coherent:")
            return x
    while not isCoherent(x):
        if verbose : 
            print("Iteration:", counter, "/ c =", ["{0:0.6f}".format(i) for i in x])
        # if some entries are < 0
        if any(t < 0 for t in x) :
            difference = 0
            for i in range(dim) :
                if allowed[i] :
                    if x[i] < 0 :
                        # difference is a positive number
                        difference = difference - x[i]
                        allowed[i] = False
                        if verbose : print("Axis", i, "blocked.")
                        x[i] = 0
            # now all initially negative entries are zero
            # compute new weights
            lmbdMod = copy.deepcopy(lmbd)
            for i in range(len(lmbdMod)) :
                if not allowed[i] : lmbdMod[i] = 0
            newLmbdSum = sum(lmbdMod)
            for i in range(len(lmbdMod)) :
                if allowed[i] :
                    lmbdMod[i] = lmbdMod[i] + ((1 - newLmbdSum) / allowed.count(True))
            if verbose : print("Current lambda:", lmbdMod)
            for i in range(dim) :
                if allowed[i] :
                    x[i] = x[i] - difference * ((1 - lmbdMod[i]) / (allowed.count(True) - sum(lmbdMod)))
        # now: if some entries are > 1
        if any(t > 1 for t in x) :
            difference = 0
            for i in range(dim) :
                if allowed[i] :
                    if x[i] > 1 :
                        # difference is a positive number
                        difference = difference + (x[i] - 1)
                        allowed[i] = False
                        if verbose : print("Axis", i, "blocked.")
                        x[i] = 1
            # now all entries initially above 1 are 1
            # compute new weights
            lmbdMod = copy.deepcopy(lmbd)
            for i in range(len(lmbdMod)) :
                if not allowed[i] : lmbdMod[i] = 0
            newLmbdSum = sum(lmbdMod)
            for i in range(len(lmbdMod)) :
                if allowed[i] :
                    lmbdMod[i] = lmbdMod[i] + ((1 - newLmbdSum) / allowed.count(True))
            if verbose : print("Current lambda:", lmbdMod)
            for i in range(dim) :
                if allowed[i] :
                    x[i] = x[i] + difference * lmbdMod[i]
                    
    if verbose : print(["{0:0.6f}".format(i) for i in x], "Sum of c: ", sum(x))
    return x
