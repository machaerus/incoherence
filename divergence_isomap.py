#!/usr/bin/env python

import numpy as np
import matplotlib
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import colormaps as cmaps
import incoherence as inc
from matplotlib import rc

rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rc('font', size='14')

# divergence
divergence = "ikld"
# precision
precision = 0.001
# resolution
res = 200
# starting credence function
c = np.array([0.9, 0.4])
# c = np.array([0.2, 0.5])
# grid
x = np.linspace(0.0001, 1, res)
y = np.linspace(0.0001, 1, res)
# probabilities
Pr = np.array([[],[]])
# map of values
isomap = np.zeros( (np.size(x), np.size(y)) )

for i in range(res) :
	for j in range(res) :
		if divergence == "gkld":
			isomap[j,i] = inc.gkld( np.array([x[i], y[j]]), c, log='e' )
		elif divergence == "igkld":
			isomap[j,i] = inc.igkld( np.array([x[i], y[j]]), c, log='e' )
		elif divergence == "kld":
			isomap[j,i] = inc.kld( np.array([x[i], y[j]]), c, log='e' )
		elif divergence == "ikld":
			isomap[j,i] = inc.ikld( np.array([x[i], y[j]]), c, log='e' )
		elif divergence == "pnorm":
			isomap[j,i] = inc.pnorm( np.array([x[i], y[j]]), c, 2 )

		if abs(x[i] + y[j] - 1) < precision :
			Pr = np.hstack( (Pr, np.array([[x[i]], [y[j]]])) )

# closest coherent
closest = inc.incoh(c, method=divergence).x

X, Y = np.meshgrid(x, y)

#################################
#  PLOT 				        #
#################################
fig = plt.figure(figsize=(9,9))
ax = fig.add_subplot(111)

# coherent functions
ax.plot(Pr[0,:], Pr[1,:],
	color='black', linewidth=1.7)

# projection line
path = ((c[0], closest[0]), (c[1], closest[1]))
ax.plot(*path, linestyle='dashed', color='grey')

# points
ax.plot(
	c[0], c[1], 'ko', markeredgecolor='w',
	markeredgewidth=3, markersize=12 )
ax.plot(
	closest[0], closest[1], 'ro', markeredgecolor='w',
	markeredgewidth=3, markersize=12 )

# divergence contour plot
CS = ax.contour(X, Y, isomap, 50, cmap = cm.Greys_r, linewidths=1)

ax.set_ylim([0, 1])
ax.set_xlim([0, 1])
ax.clabel(CS, inline=1, fontsize=12)
if divergence == "gkld":
	plt.title('Generalized Kullback-Leibler divergence')
	plt.savefig("./plots/gkld.pdf", format="pdf", bbox_inches='tight')
elif divergence == "igkld":
	plt.title('Inverse generalized Kullback-Leibler divergence')
	plt.savefig("./plots/igkld.pdf", format="pdf", bbox_inches='tight')
elif divergence == "pnorm":
	plt.title('Squared Euclidean distance')
	plt.savefig("./plots/brier.pdf", format="pdf", bbox_inches='tight')
elif divergence == "kld":
	plt.title('Kullback-Leibler divergence')
	plt.savefig("./plots/kld.pdf", format="pdf", bbox_inches='tight')
elif divergence == "ikld":
	plt.title('Inverse Kullback-Leibler divergence')
	plt.savefig("./plots/ikld.pdf", format="pdf", bbox_inches='tight')

plt.show()
