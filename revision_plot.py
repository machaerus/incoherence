#!/usr/bin/env python

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import gridspec
# from matplotlib2tikz import save as tikz_save
import ternary
import incoherence as inc

from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)

def scale(c) :
	"""Return perspective projection of belief function c
	onto the simplex in the direction of 0.
	Equivalent to scaling and to minimizing KLD/GKLD.
	"""
	return list(map(lambda x: x / np.sum(c), c))

plt.rc('text', usetex=True)
plt.rc('font', family='serif')

f, ax = plt.subplots(1,3,sharey=False,figsize=(10, 5))

########################################
# incoherence elimination
# 1: brier
########################################

ax[0].plot([1,0], [0,1], color='black', linewidth=1.5)
ax[0].annotate(r"$w_1$", xy=(1,0), xytext=(1.03,-0.08))
ax[0].annotate(r"$w_2$", xy=(0,1), xytext=(-0.08,1.03))
# bottom
bottom = [[0.1, 0.1], [0.1, 0.2], [0.1, 0.3], [0.1, 0.4], [0.1, 0.5],
        [0.1, 0.6], [0.1, 0.7], [0.1, 0.7], [0.1, 0.8], [0.2, 0.1],
        [0.3, 0.1], [0.4, 0.1], [0.5, 0.1], [0.6, 0.1], [0.7, 0.1],
        [0.8, 0.1], [0.9, 0.1]]
l = [0.5, 0.5]
bottomx = list(map(lambda x: inc.incohImaging(x,l,verbose=False), bottom))
for i in range(len(bottom)) :
    ax[0].annotate("",
        xytext=(bottom[i][0], bottom[i][1]),
        xy=(bottomx[i][0], bottomx[i][1]),
        xycoords='data',
        textcoords='data',
        arrowprops=dict(arrowstyle="->",
                        connectionstyle="arc3"))
# top
top = [[0.9, 0.1], [0.9, 0.2], [0.9, 0.3], [0.9, 0.4], [0.9, 0.5],
        [0.9, 0.6], [0.9, 0.7], [0.9, 0.7], [0.9, 0.8], [0.2, 0.9],
        [0.3, 0.9], [0.4, 0.9], [0.5, 0.9], [0.6, 0.9], [0.7, 0.9],
        [0.8, 0.9], [0.9, 0.9]]
l = [0.5, 0.5]
topx = list(map(lambda x: inc.incohImaging(x,l,verbose=False), top))
for i in range(len(top)) :
    ax[0].annotate("",
        xytext=(top[i][0], top[i][1]),
        xy=(topx[i][0], topx[i][1]),
        xycoords='data',
        textcoords='data',
        arrowprops=dict(arrowstyle="->",
                        connectionstyle="arc3"))
ax[0].set_ylim([0, 1])
ax[0].set(adjustable='box-forced', aspect='equal')
ax[0].xaxis.set_ticklabels([])
ax[0].yaxis.set_ticklabels([])
ax[0].set_title(r"a) XIM, $\lambda = (.5, .5)$")

########################################
# incoherence elimination
# 2: lewis
########################################

ax[1].plot([1,0], [0,1], color='black', linewidth=1.5)
ax[1].annotate(r"$w_1$", xy=(1,0), xytext=(1.03,-0.08))
ax[1].annotate(r"$w_2$", xy=(0,1), xytext=(-0.08,1.03))
# bottom
bottom = [[0.1, 0.1], [0.1, 0.2], [0.1, 0.3], [0.1, 0.4], [0.1, 0.5],
        [0.1, 0.6], [0.1, 0.7], [0.1, 0.7], [0.1, 0.8]]
l = [1, 0]
bottomx = list(map(lambda x: inc.incohImaging(x,l,verbose=False), bottom))
for i in range(len(bottom)) :
    ax[1].annotate("",
        xytext=(bottom[i][0], bottom[i][1]),
        xy=(bottomx[i][0], bottomx[i][1]),
        xycoords='data',
        textcoords='data',
        arrowprops=dict(arrowstyle="->",
                        connectionstyle="arc3"))
# top
top = [[0.2, 0.9], [0.3, 0.9], [0.4, 0.9], [0.5, 0.9], [0.6, 0.9], [0.7, 0.9],
        [0.8, 0.9], [0.9, 0.9]]
l = [1, 0]
topx = list(map(lambda x: inc.incohImaging(x,l,verbose=False), top))
for i in range(len(top)) :
    ax[1].annotate("",
        xytext=(top[i][0], top[i][1]),
        xy=(topx[i][0], topx[i][1]),
        xycoords='data',
        textcoords='data',
        arrowprops=dict(arrowstyle="->",
                        connectionstyle="arc3"))
ax[1].set_ylim([0, 1])
ax[1].set(adjustable='box-forced', aspect='equal')
ax[1].xaxis.set_ticklabels([])
ax[1].yaxis.set_ticklabels([])
ax[1].set_title(r"b) XIM, $\lambda = (1, 0)$")

########################################
# incoherence elimination
# 3: bayes
########################################

ax[2].plot([1,0], [0,1], color='black', linewidth=1.5)
ax[2].annotate(r"$w_1$", xy=(1,0), xytext=(1.03,-0.08))
ax[2].annotate(r"$w_2$", xy=(0,1), xytext=(-0.08,1.03))
# bottom
bottom = [[0.1, 0.1], [0.1, 0.2], [0.1, 0.3], [0.1, 0.4], [0.1, 0.5],
        [0.1, 0.6], [0.1, 0.7], [0.1, 0.7], [0.1, 0.8], [0.2, 0.1],
        [0.3, 0.1], [0.4, 0.1], [0.5, 0.1], [0.6, 0.1], [0.7, 0.1],
        [0.8, 0.1], [0.9, 0.1]]
l = [0.5, 0.5]
bottomx = list(map(scale, bottom))
for i in range(len(bottom)) :
    ax[2].annotate("",
        xytext=(bottom[i][0], bottom[i][1]),
        xy=(bottomx[i][0], bottomx[i][1]),
        xycoords='data',
        textcoords='data',
        arrowprops=dict(arrowstyle="->",
                        connectionstyle="arc3"))
# top
top = [[0.9, 0.1], [0.9, 0.2], [0.9, 0.3], [0.9, 0.4], [0.9, 0.5],
        [0.9, 0.6], [0.9, 0.7], [0.9, 0.7], [0.9, 0.8], [0.2, 0.9],
        [0.3, 0.9], [0.4, 0.9], [0.5, 0.9], [0.6, 0.9], [0.7, 0.9],
        [0.8, 0.9], [0.9, 0.9]]
l = [0.5, 0.5]
topx = list(map(scale, top))
for i in range(len(top)) :
    ax[2].annotate("",
        xytext=(top[i][0], top[i][1]),
        xy=(topx[i][0], topx[i][1]),
        xycoords='data',
        textcoords='data',
        arrowprops=dict(arrowstyle="->",
                        connectionstyle="arc3"))
ax[2].set_ylim([0, 1])
ax[2].set(adjustable='box-forced', aspect='equal')
ax[2].xaxis.set_ticklabels([])
ax[2].yaxis.set_ticklabels([])
ax[2].set_title(r"c) XBC")

# plt.savefig("/home/johann/Dropbox/praca licencjacka/szkic/images/kostka.pdf", format="pdf", bbox_inches='tight')
plt.show()
# tikz_save('mytikz.tex');
